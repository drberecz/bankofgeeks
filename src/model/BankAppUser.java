package model.pojos;

import java.io.Serializable;
import view.FrameableText;



public class BankAppUser implements Serializable, FrameableText{

    protected String name;
    protected String password;

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }
    
    
    
}
