package model.pojos;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import view.FrameableText;




public class TransferUnit implements Serializable,FrameableText{

    private static final String STATUS_SENT = "küldve";
    private static final String STATUS_RECEIVED = "fogadva";
    private static final String STATUS_REJECTED = "rejected";

    private final String STATUS;
    private final String OTHER_PARTY;
    private final double VALUE;
    private final Currency CURRENCY;
    private final LocalDate LOCAL_DATE;
    private final LocalTime TIME;  
    public TransferUnit(String OTHER_PARTY, double VALUE,Currency CURRENCY) {

        if ( VALUE ==0){
        this.STATUS = STATUS_REJECTED;    
        }else{
        this.STATUS = (VALUE<0) ? STATUS_SENT : STATUS_RECEIVED;            
        }

        this.OTHER_PARTY = OTHER_PARTY;
        this.VALUE = VALUE;
        this.CURRENCY = CURRENCY;                
        this.LOCAL_DATE = LocalDate.now();  
        this.TIME = LocalTime.now(); 
    }

    @Override
    public String toString() {
            // String HIGHLIGHTER = (STATUS_REJECTED.equals(STATUS)) ? ANSI_YELLOW_BG : ANSI_GREEN_BG;
        return "" + "STÁTUSZ=" + STATUS + ", MÁSIK FÉL=" + OTHER_PARTY + 
                ", ÉRTÉK=" + VALUE + " " + CURRENCY.getCode() + ",   " + LOCAL_DATE + "," + TIME + '}';
    }






    
    
    
    
}
