package model.pojos;

import java.io.Serializable;
import java.util.Objects;


public class Currency  implements Serializable{
       protected String code;
       protected double valueInHUF;

    public String getCode() {
        return code;
    }

    public double getValueInHUF() {
        return valueInHUF;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Currency other = (Currency) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }


}
