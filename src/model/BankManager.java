package model.pojos;


import model.Account;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import main.Main;


/**
 *
 * @author Csaba
 */
public class BankManager extends BankEmployee {

  static Scanner sc = Main.sc;
    
    private List<BankEmployee> employee = new ArrayList<>();

    public BankManager() {
    }

    public BankManager(String name, String password) {
        super();
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    public List BankEmployee() {

        return employee;
    }

    
public  void banClientAccount(Account accountToBan) {
                accountToBan.setIsActive(false);
    }
    
}
