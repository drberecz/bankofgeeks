package model.pojos;

import model.Account;
import database.DataBase;
import java.util.ArrayList;
import java.util.List;



public class Client extends BankAppUser  {

  
    
    private List<Account> accounts = new ArrayList<>();
    
    public Client(String name, String password, Account account) {
        super();
        this.name = name;
        this.password = password;
        this.accounts.add(account);
    }

    public Client() {

    }
    
    

    public int getIndexClientAccount (long accId){
        
        int i = 0;
        for(; i<accounts.size(); ++i){
            if (  accounts.get(i).getaccountId() == accId) break;
        }
        
        return (i>=accounts.size()) ?  -1 : i;
    }

    
    
    public List getAccounts (){
        
        return accounts;
    }
    
    public void addNewAccount (DataBase dBase,Account acc){
        accounts.add(acc);
        dBase.addToAccounts(acc);
        dBase.updateClient(this.name, this);
        System.out.println("\nDEBUG: Milyen névvel küldi tovább az adatbázisba?: "+ this.name);
    }

    
    
    
    @Override
    public String toString() {
        return "Client{" + "name=" + name + " password=" + password;
    }
    
    
    
}
