package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import model.pojos.Currency;
import model.pojos.TransferUnit;
import view.FrameableText;




public class Account implements Serializable,FrameableText {


private long accountID;
private String owner;
private double balance;
private Currency currency;
private  List <TransferUnit> logBook = new ArrayList<>();
private boolean active;


public Account (long accountNum, String owner, Currency currency){

    accountID = accountNum;
    balance = 999D;
    this.owner = owner;
    this.currency = currency;
    active = true;
}

    public Account() {
    }

    
    public void addToAccountLog (TransferUnit unit){
        
       logBook.add(unit); 
    } 

    public List<TransferUnit> getLogBook() {
        return logBook;
    }
    

    
public long getaccountId(){
    return this.accountID;
}

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double newBalance) {
         this.balance = newBalance;
    }
    
    public Currency getCurrency() {
        return currency;
    }

    public boolean isActive() {
        return active;
    }

    public void setIsActive(boolean isActive) {
        this.active = isActive;
    }


    
    

    @Override
    public String toString() {
        String HIGHLIGHTER = (active) ? ANSI_GREEN_BG : ANSI_RED_BG;
        String status = (active) ? "Aktiv" : "Letiltva";
        return "{" + "ID=" + accountID + HIGHLIGHTER + 
        " Állapot "+ status+  ANSI_RESET +" tulaj=" + owner + ", balance=" + 
          balance + ", currency=" + currency.getCode() + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Account other = (Account) obj;
        if (this.accountID != other.accountID) {
            return false;
        }
        return true;
    }


}




