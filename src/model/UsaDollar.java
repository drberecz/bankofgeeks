package model.pojos;


public class UsaDollar extends Currency {
    public static double currentValue = 265.5;
    
    public UsaDollar(){
        super();
        code = "USD";
        valueInHUF = currentValue;
     
        
        
    }

    public static void setCurrentValue(double currentValue) {
        UsaDollar.currentValue = currentValue;
    }
}