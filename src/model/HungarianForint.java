package model.pojos;


public class HungarianForint extends Currency {
    
    public static final double VALUE = 1.0;
    
    public HungarianForint(){
        super();
        code = "HUF";
        valueInHUF = VALUE;
        
    }
}