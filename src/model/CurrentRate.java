package model;



import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Mr. Lovasz
 */
public class CurrentRate {

    public static  double CurrencyMeanValueXMLParser(String currencyToFind,final String FILE_PATH) {
        

        double currValue = 0.0;
//./assets/arfolyam_mnb.xml
        try {
            File inputFile = new File(FILE_PATH);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("item");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    for (int i = 0; i < currencyToFind.length(); i++) {
                        if (eElement.getElementsByTagName("penznem").item(0).getTextContent().equals(currencyToFind)) {
                            currValue = Double.valueOf(eElement.getElementsByTagName("kozep").item(0).getTextContent());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
 return currValue;
    }
}


