package model.pojos;


public class Euro extends Currency {
    public static  double currentValue = 322.4D;
    
    public Euro(){
        super();
        code = "EUR";
        valueInHUF = currentValue;
        
    }

    public static void setCurrentValue(double currentValue) {
        Euro.currentValue = currentValue;
    }



    
    
}