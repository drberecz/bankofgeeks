package model;


import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



public class CurrentRateAllBanks {
    public static Map harvestRatesOfOtherBanks(String currencyToFind) {

        String[] banks = {"bb", "cib", "commerz", "erste", "kdb", "kh", "mkb", "oberbank", "otp", "raiffeisen", "sopron", "fhb", "magnet", "granit", "sberbank"};
        double[][] buyAndSellRates = new double[banks.length][2];
        double maxBuyValue = 0.0;
        double minSellValue = Double.MAX_VALUE;

        try {
            File inputFile = new File("./assets/arfolyam_all.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("item");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    for (int i = 0; i < banks.length; i++) {
                        if (eElement.getElementsByTagName("bank").item(0).getTextContent().equals(banks[i])) {
                            if (eElement.getElementsByTagName("penznem").item(0).getTextContent().equals(currencyToFind)) {
                                buyAndSellRates[i][0] = Double.valueOf(eElement.getElementsByTagName("vetel").item(0).getTextContent());
                                if (buyAndSellRates[i][0] >= maxBuyValue) {
                                    maxBuyValue = buyAndSellRates[i][0];
                                }
                                buyAndSellRates[i][1] = Double.valueOf(eElement.getElementsByTagName("eladas").item(0).getTextContent());
                                if (buyAndSellRates[i][1] <= minSellValue) {
                                    minSellValue = buyAndSellRates[i][1];
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, List<Double>> data = new HashMap<String, List<Double>>();
        for (int i = 0; i < buyAndSellRates.length; i++) {
            data.put(banks[i], Arrays.asList(buyAndSellRates[i][0], buyAndSellRates[i][1]));
        }

        //System.out.println("Maximális vételi ár: " + maxBuyValue);
        //System.out.println("Minimális eladási ár: " + minSellValue);

        return data;
    }
}


