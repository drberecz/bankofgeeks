package model.pojos;

import database.DataBase;
import java.io.Serializable;
import java.util.List;
import java.util.Scanner;
import main.Main;
import view.ConsolePrinter;
/**
 *
 * @author Csaba
 */
public class BankEmployee extends BankAppUser implements Serializable{

  static Scanner sc = Main.sc;   
 
    public BankEmployee() {
    }
 
    public BankEmployee(String name, String password) {
        super();
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "BankEmployee{" + '}';
    }
   
  
public static Client browseClientAccounts(DataBase dBase) {
        String clientNameInputToBrowse = "";
        boolean ok = false;

          Client client = new Client();
        
        do{
        System.out.print("Kérem a számlatulajdonos nevét: ");
        clientNameInputToBrowse = sc.nextLine();

        
        if (dBase.containsName(clientNameInputToBrowse)) {
            
            client = dBase.getClient(clientNameInputToBrowse);
            System.out.println("Ezek " + clientNameInputToBrowse + " számlái:");
            ConsolePrinter.printListOfClientACCS(client);
            

            System.out.println("Tranzakciós listái: ");
            ConsolePrinter.printTransactionHistory(client);
            ok = true;
        } else {
            System.out.println("nincs ilyen ügyfél az adatbázisban!");
        }
        }while(!ok);
        
       return client;
    }
    
    
}