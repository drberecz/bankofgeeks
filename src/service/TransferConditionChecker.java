package model;

import control.MainMenu;
import database.DataBase;
import model.pojos.Client;
import model.pojos.Currency;
import model.pojos.TransferUnit;
import view.FrameableText;
import view.HtmlTag;
import view.MainWindow;


public class TransferConditionChecker  implements FrameableText{

    public static MainWindow mw = MainMenu.mw;


public static void evaltrsfConditions(DataBase dBase,Client currentClient, Account sourceAcc,double transferValue, long input) throws Exception{
    

        String userName = sourceAcc.getOwner();
        Currency sourceAccCurrency = sourceAcc.getCurrency();
        double sourceAccBalance = sourceAcc.getBalance();
        
        String beneficiaryName = dBase.getAccount(input).getOwner();
        Client beneficiary = dBase.getClient(beneficiaryName);      
        int targetAccIndex = beneficiary.getIndexClientAccount(input);
        Account targetAcc = (Account)beneficiary.getAccounts().get(targetAccIndex);

        Currency targetAccCurrency = targetAcc.getCurrency();
        mw.updateWindow(HtmlTag.H1, "Célszámla pénzNeme: "  +targetAccCurrency.getCode());    
    

       if ( !sourceAcc.isActive() | !targetAcc.isActive() ){
        TransferUnit sourceLog = new TransferUnit(beneficiaryName, 0, sourceAccCurrency);
        sourceAcc.addToAccountLog(sourceLog);
        dBase.updateClient(userName, currentClient);
        dBase.addToAccounts(sourceAcc);

        mw.clearWindow();
        mw.updateWindow(HtmlTag.H2, 
           "HIBA, Forrás és/vagy célszámla<br>"+
                   " le van tiltva!<br>Kérjük fáradjon be személyesen a BANK-ba" +                            ANSI_PURPLE_BG +ANSI_YELLOW+"BANK"+ ANSI_RESET +  "-ba.\n"+
                    "<br>SIKERTELEN utalas "     
                );
        throw new Exception(currentClient.convertTextToFramed(("HIBA, Forrás és/vagy célszámla"+ANSI_RED_BG+
                    " le van tiltva!"+ANSI_RESET + "\nKérjük fáradjon be személyesen a "+
                             ANSI_PURPLE_BG +ANSI_YELLOW+"BANK"+ ANSI_RESET +  "-ba.\n"+
                    ANSI_RED_BG+ "SIKERTELEN utalas "+ ANSI_RESET)));         
       }
        
       double transferValueTo = CurrencyConverter.convertCurrency(transferValue, sourceAccCurrency, targetAccCurrency);

        double targetAccBalance = targetAcc.getBalance();

        sourceAcc.setBalance(sourceAccBalance - transferValue);
        targetAcc.setBalance(targetAccBalance + transferValueTo);
        

        TransferUnit sourceLog = new TransferUnit(beneficiaryName, -transferValue, sourceAccCurrency);
        sourceAcc.addToAccountLog(sourceLog);
        TransferUnit targetLog = new TransferUnit(userName, transferValueTo, targetAccCurrency);
        targetAcc.addToAccountLog(targetLog);
        
        mw.updateWindow(HtmlTag.SPAN,currentClient.convertTextToFramed("SIKERES UTALAS<br>"));
        mw.updateWindow(HtmlTag.SPAN,sourceLog.toString());
        mw.updateWindow(HtmlTag.SPAN,targetLog.toString());

        dBase.updateClient(userName, currentClient);
        dBase.updateClient(beneficiary.getName(), beneficiary);
        dBase.addToAccounts(sourceAcc);
        dBase.addToAccounts(targetAcc);
        
    
    
    
    
    
}
    
}
