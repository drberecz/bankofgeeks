package control;

import database.DataBase;
import java.util.Scanner;
import main.Main;
import model.Account;
import model.pojos.Client;
import model.pojos.Currency;
import model.pojos.Euro;
import model.pojos.HungarianForint;
import model.pojos.UsaDollar;


public class AddNewUserAccount {

  static Scanner sc = Main.sc;
    
    
    public static void addNewUserAccount(DataBase dBase) {
        int select = 0;
        boolean ok = false;
        boolean quit = false;

        String existingClientName = "";

        while (!quit) {
            boolean isNotFirst = false;
            System.out.print("Kliens neve? : (kilépés (K)-ra)");
            existingClientName = sc.nextLine();
            if ("K".equals(existingClientName.toUpperCase())) {
                break;
            }

            if (!dBase.containsName(existingClientName)) {
                System.out.println("Hiba, nincs benne az adatbázisban a név!");

            } else {
                Client clientInstance = dBase.getClient(existingClientName);
                while (!ok) {
                    System.out.println("(1) HUF, (2) EUR, (3) USD ... ");
                    if (sc.hasNextInt()) {
                        System.out.print(isNotFirst ? "(1) HUF, (2) EUR, (3) USD ... " : "");
                        select = sc.nextInt();
                        isNotFirst = true;
                        switch (select) {
                            case 1:
                                Currency huf = new HungarianForint();
                                Account acc = new Account(dBase.highestAccountIdInDatabase() + 1, existingClientName, huf);
                                clientInstance.addNewAccount(dBase, acc);
                                dBase.addToAccounts(acc);
                                System.out.println("HUF account created!");
                                ok = true;
                                quit = ok;

                                break;
                            case 2:
                                Currency eur = new Euro();

                                Account acc1 = new Account(dBase.highestAccountIdInDatabase() + 1, existingClientName, eur);
                                clientInstance.addNewAccount(dBase, acc1);
                                dBase.addToAccounts(acc1);
                                System.out.println("EUR account created!");
                                ok = true;
                                quit = ok;

                                break;
                            case 3:
                                Currency usd = new UsaDollar();

                                Account acc2 = new Account(dBase.highestAccountIdInDatabase() + 1, existingClientName, usd);
                                clientInstance.addNewAccount(dBase, acc2);
                                dBase.addToAccounts(acc2);
                                System.out.println("USD account created!");
                                ok = true;
                                quit = ok;
                                break;
                            default:

                        }

                    } else {
                        System.out.println("hiba! ");
                        sc.next();
                    }

                }

            }
        }
        System.out.println("end of account creating!");

    }        
}
        
    

    

