package model;

import model.pojos.Currency;
import model.pojos.Euro;
import model.pojos.UsaDollar;

/**
 *
 * @author Csaba
 */
public class CurrencyConverter {

    static double meanEUR = 322.41;
    static double meanUSD = 287.22;
    static double crossEURUSD = 1.12;
    static double crossUSDEUR = 0.88;

    public static double convertCurrency(double value, Currency currencyFrom, Currency currencyTo) {

        String curr1 = currencyFrom.getCode();//A paraméterben megadott példányból getter-el 
        String curr2 = currencyTo.getCode();// kikéri a String tipusú devizakódot

        if (curr1.equals(curr2)) {  // ha egyezik a devizák típusa visszatér konvertálás nélkül
            return value;
        }

        {// ez a blokk feltölti friss értékkel a statikus változókat
        Currency eurInstance = new Euro();
        Currency usdInstance = new UsaDollar();

        meanEUR = eurInstance.getValueInHUF();
        meanUSD = usdInstance.getValueInHUF();
        crossEURUSD = meanEUR / meanUSD;
        crossEURUSD = meanUSD / meanEUR;
        }
        
        
        // itt hívja a konvertáló függvényt
        value = currency(value, curr1, curr2);

        return value;
    }

    public static double currency(double value, String curr1, String curr2) {

        if ("EUR".equals(curr1) && "USD".equals(curr2)) {
            return ConvertEurToUSdollar(value, crossEURUSD);
        }
        if ("USD".equals(curr1) && "EUR".equals(curr2)) {
            return ConvertUsDollarToEur(value, crossUSDEUR);
        }
        if ("HUF".equals(curr1) && "EUR".equals(curr2)) {
            return ConvertHungarianForintToEUR(value, 1D/meanEUR);
        }
        if ("EUR".equals(curr1) && "HUF".equals(curr2)) {
            return ConvertEurToHungarianForint(value, meanEUR);
        }
        if ("HUF".equals(curr1) && "USD".equals(curr2)) {
            return ConvertHungarianForintToUsDollar(value, 1/meanUSD);
        }
        if ("USD".equals(curr1) && "HUF".equals(curr2)) {
            return ConvertUsDollarHungarianForint(value, meanUSD);
        }

        return 0;

    }

    private static double ConvertEurToUSdollar(double value, double cRate) {
        return cRate * value;

    }

    private static double ConvertUsDollarToEur(double value, double cRate) {
        return cRate * value;
    }

    private static double ConvertHungarianForintToEUR(double value, double cRate) {
        return cRate * value;
    }

    private static double ConvertEurToHungarianForint(double value, double cRate) {
        return cRate * value;
    }

    private static double ConvertHungarianForintToUsDollar(double value, double cRate) {
        return cRate * value;
    }

    private static double ConvertUsDollarHungarianForint(double value, double cRate) {
        return cRate * value;
    }
}
