package model;


import java.util.Scanner;
import main.Main;

/**
 *
 * @author Csaba
 */
public class EvalPasswordStrength {
    
    
    static Scanner sc = Main.sc;
    
    private static final int PASSWORD_LENGTH = 6;
    
    
    
    public static String checkPassword () {
    
        
     String password = "";   
    boolean ok = false;
        do {
        System.out.println("Jelszó? ");
        
        password = sc.nextLine();
        if (isValidPassWord(password)){
        
        ok = true;
           
        System.out.println("A jelszó elég erős, létrejött az új felhasználó");
        
        } else {
            System.out.println("A jelszó nem elég erős");
            System.out.println(PASSWORD_LENGTH +" Karakternek kell lennie");
            System.out.println("Kell bele nagybetű és szám is");
        }
        }
     while (!ok);   
        return password;
    } 
    
    static boolean isValidPassWord(String password){
        if (password.length() < PASSWORD_LENGTH) 
            return false;
        
        int charCount =0;
        int numCount =0;
        for (int i = 0; i < password.length(); i++) {

            char ch = password.charAt(i);
            
            if (isNumeric(ch)) numCount++;
            else if (isLetter(ch)) charCount++;
            else return false;
        
    }
        return (charCount >= 2 && numCount >= 2);
    }
    
    private static boolean isNumeric(char ch) {
        return (ch >= '0' && ch <= '9');
    }
    
    private static boolean isLetter(char ch) {
        ch = Character.toUpperCase(ch);
        return (ch >= 'A' && ch <= 'Z');
    }
    
}

