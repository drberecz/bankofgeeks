package main;

import control.HtmlFilter;
import control.utils.RSSparser;
import java.util.List;
import java.util.Scanner;
import view.ConsolePrinter;
import view.HtmlViewer;
import view.SwingGui;
import view.MainWindow;

public class Main {

    public static Scanner sc = new Scanner(System.in);

    public static HtmlViewer popUpWindow = null;
    
    
    public static void main(String[] args) throws Exception {

        SwingGui.initDisplay();

        
         ConsolePrinter.printStartScreen();
 
         String URL = "http://hvg.hu/rss/gazdasag";
        List<String> webContent = RSSparser.downloadContent(URL);
        String content = HtmlFilter.filterHtmlContent(webContent);

        HtmlViewer.setContent(content);
        Thread.sleep(10000);
        popUpWindow = HtmlViewer.invokeConstructor(); 

    }
}
