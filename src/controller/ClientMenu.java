package control;

import database.DataBase;
import java.util.List;
import java.util.Scanner;
import main.Main;
import model.Account;
import model.pojos.Client;
import model.pojos.TransferUnit;
import view.ConsolePrinter;
import view.FrameableText;
import view.HtmlTag;
import view.MainWindow;

public class ClientMenu implements FrameableText{

    public static MainWindow mw = MainMenu.mw;

    
    public static void clientMenu(DataBase dBase, Client currentClient) throws InterruptedException {

        String inputline = "";
        
        mw.updateWindow(HtmlTag.H1, 
                ("ÜGYFÉL - " + currentClient.getName()));

        do {
            do {
                mw.updateWindow(HtmlTag.P,"(E)gyenleg/history lekérdezés , (U)talás, (K)ilépés");
                inputline = mw.wait4Input().toUpperCase();
            } while (!"E".equals(inputline) && !"U".equals(inputline) && !"K".equals(inputline));

            switch (inputline) {
                case "U":
                  //  mw.updateWindow(HtmlTag.H2, "BOCSI, NINCS KESZ");
                    try{
                    TransfersMenu.transfersMenu(dBase, currentClient);
                    }catch(Exception e){
           
               mw.updateWindow(HtmlTag.H2,"HIBA TÖRTÉNT, HELYTELEN ADAT, VISSZALÉP AZ ÜGYFÉLMENÜBE . . . " ) ;               

                    }
                    break;
                case "E":
                    mw.updateWindow( HtmlTag.H1, "Ezek a számláid adatai");

                    ConsolePrinter.printListOfClientACCS(currentClient);
                    ConsolePrinter.printTransactionHistory(currentClient);
            }

        } while (!"K".equals(inputline));

    }

}

