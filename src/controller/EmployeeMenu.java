package control;

import static control.AddNewUserAccount.addNewUserAccount;
import database.DataBase;
import java.util.Scanner;
import main.Main;
import model.Account;
import model.pojos.BankEmployee;
import model.pojos.Client;
import model.pojos.Currency;
import model.pojos.HungarianForint;
import view.ConsolePrinter;
import view.FrameableText;

public class EmployeeMenu  implements FrameableText{

    static Scanner sc = Main.sc;

    public static void employeeMenu(DataBase dBase, BankEmployee currentEmp) {
        String request;

        System.out.println("Dolgozó - " + currentEmp.getName());
        
        do {
            System.out.println("(U)j ügyfél felvitele, (L) Ügyfél számlák Listázása, (S)zámla hozzádása (K)ilépés");

            request = sc.nextLine().toUpperCase();
            switch (request) {
                case "U":
                    System.out.println("Új felhasználó felvitele...");
                    createNewClient(dBase);
                    break;
                case "L":
                    ConsolePrinter.printClients(dBase);
                    BankEmployee.browseClientAccounts(dBase);
                    break;

                case "S":
                    System.out.println("Számla  hozzáadása...");
                    addNewUserAccount(dBase);
                    break;

            }
        } while (!"K".equals(request));
        System.out.println("TERJ VISSZA AZ ABLAK RA");
    }

    public static void createNewClient(DataBase dBase) {
        String inputLine = "";
        boolean ok = false;
        do {
            System.out.println("\nird be az új Kliens nevet:");
            inputLine = sc.nextLine();
            if (inputLine.length() < 3) {
                System.out.println("Hiba, legalabb 3 karakter hosszu nev kell!");
            } else {
                ok = true;
            }
        } while (!ok);

        String password =  model.EvalPasswordStrength.checkPassword();
        
        Currency huf = new HungarianForint();
        Account acc = new Account(dBase.highestAccountIdInDatabase() + 1, inputLine, huf);
        dBase.updateClient(inputLine, new Client(inputLine, password, acc));
        dBase.addToAccounts(acc);

        // itt egyből kikérjük a DB-ből, ellenőrzés végett
        System.out.println("Új Ügyfel felvitele sikerült.\n");
        Client clientInstance = dBase.getClient(inputLine);
        System.out.println(clientInstance.toString());
        ConsolePrinter.printListOfClientACCS(clientInstance);
    }

}
