package control;

import database.DataBase;
import java.util.Scanner;
import main.Main;
import model.TransferConditionChecker;
import model.Account;
import model.pojos.Client;
import model.pojos.Currency;
import view.ConsolePrinter;
import view.FrameableText;
import view.HtmlTag;
import view.MainWindow;

public class TransfersMenu implements FrameableText {

    static Scanner sc = Main.sc;
    public static MainWindow mw = MainMenu.mw;
    /*
  paraméterek
  Az utaláshoz ismernünk kell a küldő fél adatait - currentClient
  a dBase példány meg azé' kell, kivenni belőle a fogadó fél adatait
     */
    public static void transfersMenu(DataBase dBase, Client currentClient) throws Exception {
mw.clearWindow();
        mw.updateWindow(HtmlTag.H1, "UTALAS menu");
        
        
        ConsolePrinter.printListOfClientACCS(currentClient);// ez a method a kliens-hez tartozik, csak a saját számlákat printeli
        String userName = currentClient.getName();
        mw.updateWindow(HtmlTag.P, userName + " ! Add meg a forrásszámla azonositóját: ");

        /*
        A forrásszámla ID long típus, a Kliens obj. arraylist-ben tárolja a számla obj.-okat
        getIndexClientAccount metódus sima lineáris keresés
        csak adatbevitellel kombinálva, akkor enged tovább, ha van a kliens-nek
        adott számlája, pl. 1001
         */
        int sourceAccIndex = -1;
        do {

 long input = Long.parseLong(mw.wait4Input());
            if (currentClient.getIndexClientAccount(input) >= 0) {
                sourceAccIndex = currentClient.getIndexClientAccount(input);
            } else {
                mw.updateWindow(HtmlTag.H2,"Nincs ilyen szamlád! Újra kérem");
            }
        } while (sourceAccIndex == -1);

        /*
        most beteszem egy változóba a számla obj.-t sourceAcc, ez NEM új példány, csak ref.
        a Currency szintén NEM új példány, ref.
         */
        Account sourceAcc = (Account) currentClient.getAccounts().get(sourceAccIndex);
        Currency sourceAccCurrency = sourceAcc.getCurrency();

        if ( sourceAcc.getBalance()<=0.0) throw new Exception("MONDOM hogy URES");
        
        
        
        mw.updateWindow(HtmlTag.H1,"Forrásszámla pénzNeme: " + sourceAccCurrency.getCode());
        mw.updateWindow(HtmlTag.P, userName + " ! Add meg az utalandó összeget: ");

        double transferValue = 0;
        double sourceAccBalance = sourceAcc.getBalance();// itt eltárolom az egyenleget
        boolean ok = false;
        do {
            double input = Double.parseDouble(mw.wait4Input()); // ellenőrzött adatbevitel
            if (input <= 0D | input > sourceAccBalance) {
                mw.updateWindow(HtmlTag.H2,"Problem!, vagy negativ az összeg, vagy nincs fedezet");
                mw.updateWindow(HtmlTag.P,"Ennyi van a forrasszamlan: " + sourceAccBalance + " " + sourceAccCurrency);
            } else {
                transferValue = input;
                ok = true;
            }

        } while (!ok);

        mw.clearWindow();;
        ConsolePrinter.printAccounts(dBase);// minden számlát kiprintel, hogy könnyű legyen választani
        mw.updateWindow(HtmlTag.P, userName + " ! Add meg a celszámla azonositóját: ");
        /*
        hogy tárolja a database a számlákat? . . . az az Ő dolga
        annyit enged, hogy megnézzem , létezik e a célszámla:containsAccountId(input)
        ez a kódrész kiszűri azt a bakit, ha ugyanarra a számlára utalna az ügyfél
         */
        long input = -1L;
        boolean isSameAccount;
        do {
            input = Long.parseLong(mw.wait4Input());
            if (!dBase.containsAccountId(input)) {
                mw.updateWindow(HtmlTag.H2,"Nincs ilyen szamla az adatbazis-ban! Ujra kerem");
            }
            isSameAccount = false;
            if (sourceAcc.equals(dBase.getAccount(input))) {
                mw.updateWindow(HtmlTag.H2,"Hiba, ugyanarra a szamlara probalsz utalni");
                isSameAccount = true;
            }
        } while (!dBase.containsAccountId(input) || isSameAccount);

        try {

            TransferConditionChecker.evaltrsfConditions(dBase, currentClient, sourceAcc, transferValue, input);

        } catch (Exception e) {
            System.out.println(e);
        }

    }

}
