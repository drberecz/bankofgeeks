package control;

import database.DataBase;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import main.Main;
import model.Account;
import model.pojos.BankAppUser;
import model.pojos.BankEmployee;
import model.pojos.BankManager;
import model.pojos.Client;
import view.ConsolePrinter;
import view.FrameableText;

public class BankManagerMenu implements FrameableText {

    static Scanner sc = Main.sc;

    public static Map<String, BankEmployee> employeeMap = new HashMap<>();

    static {
        employeeMap.put("Gizike", new BankEmployee("Gizike", "pass"));
        employeeMap.put("Jolika", new BankEmployee("Jolika", "pass"));
        employeeMap.put("Jocika", new BankEmployee("Jocika", "pass"));
        employeeMap.put("Csaba", new BankManager("Csaba", "pass"));
    }

    public static void bankManagerMenu(BankEmployee currentUser, DataBase dBase) {

        BankManager manager = (BankManager) currentUser;

        currentUser.AccessLevelFramed("MANAGER - " + currentUser.getName());

        String request;

        do {
            System.out.println("(U)j dolgozo felvitele, (L) Ügyfél számlák Listázása, (T)Ügyfél számla letiltás (K)ilépés");

            request = sc.nextLine().toUpperCase();
            switch (request) {
                case "U":

                    String newEmpName = addNewEmployee();
                    if (employeeMap.containsKey(newEmpName)) {
                        System.out.println(currentUser.convertTextToFramed(ANSI_RED_BG + "HIBA! "
                                + ANSI_RESET + " -van már dolgozó ilyen névvel! "));
                    }
                    employeeMap.put(newEmpName, new BankEmployee(newEmpName, "pass"));
                    break;
                case "L":
                    ConsolePrinter.printClients(dBase);
                    BankEmployee.browseClientAccounts(dBase);
                    break;

                case "T":
                    ConsolePrinter.printClients(dBase);
                    Client clientToBan = BankEmployee.browseClientAccounts(dBase);
                    Account account = chooseAccountToBan(manager, clientToBan);

                    manager.banClientAccount(account);
                    System.out.println(ANSI_YELLOW + ANSI_PURPLE_BG +"LETILTÁS SIKERÜLT" + ANSI_RESET);
                    dBase.updateClient(clientToBan.getName(), clientToBan);
                    dBase.addToAccounts(account);
                    ConsolePrinter.printListOfClientACCS(clientToBan);
                    break;

            }
        } while (!"K".equals(request));

    }

    public static String addNewEmployee() {

        System.out.println("Új felhasználó felvitele...");
        String inputLine = "";
        boolean ok = false;
        do {
            System.out.println("\nird be az új Alkalmazott nevét:");
            inputLine = sc.nextLine();
            if (inputLine.length() < 3) {
                System.out.println("Hiba, legalabb 3 karakter hosszu nev kell!");
            } else {
                ok = true;
            }
        } while (!ok);
        return inputLine;

    }

    public static Account chooseAccountToBan(BankManager manager, Client clientToBan) {

        System.out.println(manager.getName() + " ! Add meg a letiltandó ügyfél számla azonositóját: ");

        int AcctoBanIndex = -1;
        do {
            long input = InputValidator.userInputValidatedAsLong(sc);
            if (clientToBan.getIndexClientAccount(input) >= 0) {
                AcctoBanIndex = clientToBan.getIndexClientAccount(input);
            } else {
                System.out.println("Nincs ilyen szamlája az ügyfélnek! Ujra kerem");
            }
        } while (AcctoBanIndex == -1);

        Account accountToBan = (Account) clientToBan.getAccounts().get(AcctoBanIndex);

        return accountToBan;
    }

}
