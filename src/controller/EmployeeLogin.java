package control;

import static control.BankManagerMenu.employeeMap;
import java.util.Scanner;
import main.Main;
import model.pojos.BankEmployee;

/**
 *
 * @author Csaba
 */
public class EmployeeLogin {
    
  static Scanner sc = Main.sc;
    

    
    
    
    public static BankEmployee empLogin() { 
    boolean ok = false;
    
    BankEmployee currentEmp = new BankEmployee();
    
    
            do {
            System.out.print("Felhasználónév: ");
            String userName = sc.nextLine();

            if (!employeeMap.containsKey(userName)) {
                System.out.println("problem, nincs benne");
            } else {
                
                currentEmp = employeeMap.get(userName);
                System.out.print("Jelszó: ");
                String passwordToCheck = sc.nextLine();

               
                if (passwordToCheck.equals(currentEmp.getPassword())) {
                    ok = true;
                    System.out.println("Dolgozói menü");

                } else {
                    System.out.println("wrong password! ");
                }
            }
        } while (!ok);
        return currentEmp;
    }
    
}
    

