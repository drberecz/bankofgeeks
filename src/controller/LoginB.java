package control;

import static control.MainMenu.mw;
import database.DataBase;
import java.util.Scanner;
import main.Main;
import model.pojos.Client;
import view.HtmlTag;
import view.MainWindow;

public class LoginB {

  static Scanner sc = Main.sc;
    public static MainWindow mw = MainMenu.mw;
  
    
    public static Client checkUserCredentials(DataBase dBase) throws InterruptedException {
      
        mw.clearWindow();
        mw.updateWindow(HtmlTag.H1,"UGYFEL MENU");
        
        Client clientToCheck = new Client();
        boolean ok = false;
        do {
            mw.updateWindow(HtmlTag.P,"Felhasználónév? ");
            String userName = mw.wait4Input();

            if (!dBase.containsName(userName)) {
                mw.updateWindow(HtmlTag.H1, "problem, nincs benne");
            } else {
                mw.updateWindow(HtmlTag.P, "Jelszó: ");
                String passwordToCheck = mw.wait4Input();

                   clientToCheck = dBase.getClient(userName);
                   String acceptablePass = clientToCheck.getPassword();
                  mw.updateWindow(HtmlTag.H2,"DEBUG: a JO JELSZO: "+ acceptablePass);
               
                if (passwordToCheck.equals(acceptablePass)) {
                    ok = true;
                    mw.updateWindow(HtmlTag.P,"login accepted!");
                } else {
                    mw.updateWindow(HtmlTag.H2,"wrong password! ");
                }
            }
        } while (!ok);
        return clientToCheck;
    }
}