package control;

import control.utils.HttpDownloadUtility;
import database.DataBase;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.Main;
import model.CurrentRate;
import model.CurrentRateAllBanks;
import model.pojos.BankEmployee;
import model.pojos.BankManager;
import model.pojos.Client;
import model.pojos.Euro;
import model.pojos.UsaDollar;
import view.ConsolePrinter;
import view.FrameableText;
import view.HtmlTag;
import view.MainWindow;
import view.SwingGui;

public class MainMenu implements FrameableText, Runnable {

//Display display = HttpDownloader.downloadImage();
    static Scanner sc = Main.sc;
    public static MainWindow mw = null;

    private static Map<String, List<Double>> ratesMapEUR;
    private static Map<String, List<Double>> ratesMapUSD;

    @Override
    public void run() {
        mw = MainWindow.invokeConstructor();

        DataBase dBase = DataBase.getInstance();

        final String ASSET_URL = "http://api.napiarfolyam.hu/?bank=mnb";
        final String DIR_PATH = "./assets/";

        String xmlFileName = "";
        try {
            xmlFileName = HttpDownloadUtility.downloadFile(ASSET_URL, DIR_PATH);
        } catch (IOException ex) {
            System.out.println("Problem, Nem sikerult letolteni az XML file-t");
        }
        System.out.println("Debug: ez az xml FILE neve: " + xmlFileName);
        double rateEUR, rateUSD;
        rateEUR = 321.0D;//beegetett EUR
        rateUSD = 267.5D;//beegetett USD
        Euro.setCurrentValue(rateEUR);
        UsaDollar.setCurrentValue(rateUSD);

        try {
//arfolyam_mnb.xml
            rateEUR = CurrentRate.CurrencyMeanValueXMLParser("EUR", DIR_PATH + xmlFileName);
            rateUSD = CurrentRate.CurrencyMeanValueXMLParser("USD", DIR_PATH + xmlFileName);
            if (rateEUR == 0.0 | rateUSD == 0.0) {
                throw new Exception(
                        "Nem sikerult letolteni az arfolyamot . . . marad a beégetett érték");
            }

            Euro.setCurrentValue(rateEUR);
            UsaDollar.setCurrentValue(rateUSD);
        } catch (Exception e) {
            System.out.println(e);
        }

        StringBuilder sb = new StringBuilder();
        ratesMapEUR = CurrentRateAllBanks.harvestRatesOfOtherBanks("EUR");
        sb.append("Aktuális EUR vételi és eladási árfolyamok:\n\n");

        for (Map.Entry<String, List<Double>> entry : ratesMapEUR.entrySet()) {
            sb.append("Bank: ").append(entry.getKey()).append("  Vetel      Eladas\n");
            List<Double> currentList = entry.getValue();
            sb.append("              ");
            sb.append(currentList.get(0));
            sb.append("   ");
            sb.append(currentList.get(1));
            sb.append("\n");
        }
        SwingGui.lists.setText(sb.toString());

        ratesMapUSD = CurrentRateAllBanks.harvestRatesOfOtherBanks("USD");
        System.out.println("Aktuális USD vételi és eladási árfolyamok: " + ratesMapUSD);

        System.out.println("\n\nEZEk a nevek vannak az adatbazisban:");
        ConsolePrinter.printClients(dBase);

        System.out.println("\n\nEZEk a Szamlak vannak az adatbazisban:");
        ConsolePrinter.printAccounts(dBase);

        mw.updateWindow(HtmlTag.H2,
                "\n\nKOCKÁK BANKJA - NÁLUNK NINCS ÁRFOLYAMKOCKÁZAT");

        String inputline = "";

        do {
            do {
                mw.clearWindow();
                mw.updateWindow(HtmlTag.H1,
                        "    Bank FŐMENÜ    ");
                mw.updateWindow(HtmlTag.H1,
                        "NINCS BEJELENTKEZVE");
                mw.updateWindow(HtmlTag.H2,
                        "\nKérem válasszon menüpontot:");
                mw.updateWindow(HtmlTag.P,
                        "(D)olgozó bejelentkezés | (U)gyfél bejelentkezés | (V)ege");
                try {
                    inputline = MainWindow.wait4Input().toUpperCase();
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
                }
            } while (!"D".equals(inputline) && !"U".equals(inputline) && !"V".equals(inputline));

            switch (inputline) {
                case "D":
                    mw.clearWindow();
                    mw.updateWindow(HtmlTag.H1, "A DOLGOZÓI MENÜT KONZOLRÓL HASZNÁLHATOD ");

                    BankEmployee currentEmp = EmployeeLogin.empLogin();
                    if (currentEmp instanceof BankManager) {
                        BankManagerMenu.bankManagerMenu(currentEmp, dBase);
                    } else {
                    EmployeeMenu.employeeMenu(dBase, currentEmp);
                    }
                    break;
                case "U":
                    Client currentClient = null;
                    try {
                        currentClient = LoginB.checkUserCredentials(dBase);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
                    }
            {
                try {
                    ClientMenu.clientMenu(dBase, currentClient);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            }
        } while (!"V".equals(inputline));
        mw.clearWindow();
        mw.updateWindow(HtmlTag.H1,
                "VEGE A PROGRAMNAK. BYE-BYE!");
    }
}
