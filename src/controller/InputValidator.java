package control;

import java.util.Scanner;



public class InputValidator {


    public static long userInputValidatedAsLong (Scanner sc){

        long input = Integer.MIN_VALUE;
        boolean ok = false;
        
        while ( !ok){
            try{
               input =  sc.nextLong();
               ok = true;
            }
            catch (Exception e){
                sc.next();
                System.out.println("Helytelen adatbevitel! Számot kérek.");
            }
        }
               
    return input;        
    }

    public static double userInputValidatedAsDouble (Scanner sc){

        double input = Double.MIN_VALUE;
        boolean ok = false;
        
        while ( !ok){
            try{
               input =  sc.nextDouble();
               ok = true;
            }
            catch (Exception e){
                System.out.println("Helytelen adatbevitel! Számot kérek.");
                sc.next();
            }
        }
               
    return input;        
    }
    

}
