package database;

import control.utils.FileIO;
import java.util.HashMap;
import java.util.Map;
import model.Account;
import model.pojos.Client;
import model.pojos.HungarianForint;
import view.FrameableText;

public class DataBase implements FrameableText{

    
    static DataBase instance = null; 
    
    public final static String FILENAME_CLIENT = "clients.ser";
    public final static String FILENAME_ACCOUNTS = "accounts.ser";
    public final static String FILENAME_BANKEMPLOYEE = "employees.ser";
    private Map<String, Client> mapOfClients = new HashMap<>();
    private Map<Long, Account> mapOfAccounts = new HashMap<>();

    
 
    static public DataBase getInstance() 
    { 
        if (instance == null)         
             instance = new DataBase(); 
   
        return instance; 
    }     
    
    
    
    private DataBase() {
        FileIO.createDir();
        try {

            mapOfClients = (Map<String, Client>) FileIO.readFile(FILENAME_CLIENT);
        } catch (Exception e) {
            System.out.println("Database UZENET>AJJAJJJ, kis problema van: nincs lmentve client " + e);
             fillWithDummyValues();
        }
        FileIO.writeFile(mapOfClients, FILENAME_CLIENT);

        try {

            mapOfAccounts = (Map<Long, Account>) FileIO.readFile(FILENAME_ACCOUNTS);
        } catch (Exception e) {
            System.out.println("Database UZENET> problema van: nincs account lementve" + e);
        }
        FileIO.writeFile(mapOfAccounts, FILENAME_ACCOUNTS);

    }

    
    public long highestAccountIdInDatabase(){
        long maxValue = Long.MIN_VALUE;
        for (long accIdNum : mapOfAccounts.keySet()){
            if ( accIdNum > maxValue) maxValue = accIdNum;
        }
        
        
        return maxValue;
    }
    
    



    public void addToAccounts(Account account) {

        mapOfAccounts.put(account.getaccountId(), account);
        FileIO.writeFile(mapOfAccounts, FILENAME_ACCOUNTS);
    }

    public void updateClient(String name, Client client) {

        mapOfClients.put(name, client);
        FileIO.writeFile(mapOfClients, FILENAME_CLIENT);
    }

    public Map<String, Client> getClientMap() {
        return mapOfClients;
    }

    public Map<Long, Account> getAccountMap() {
        return mapOfAccounts;
    }

    public final void fillWithDummyValues() {
        HungarianForint assetType = new HungarianForint();
 
        System.out.println("WARNING' database FILE nem volt, beégetek 4 klienst");   
        Account acc1 = new Account(1000L, "Bela", assetType);
        Account acc2 = new Account(1001L, "Cecil", assetType);
        Account acc3 = new Account(1002L, "Denes", assetType);
        Account acc4 = new Account(1003L, "Dr.Genya", assetType);
        mapOfClients.put("Bela", new Client("Bela", "pass", acc1));

        addToAccounts(acc1);
        mapOfClients.put("Cecil", new Client("Cecil", "pass", acc2));
        
        addToAccounts(acc2);
        mapOfClients.put("Denes", new Client("Denes", "pass", acc3));
        addToAccounts(acc3);

        mapOfClients.put("Dr.Genya", new Client("Dr.Genya", "pass", acc4));
        addToAccounts(acc4);

    }
    
    
    public  Client getClient(String name){
           
        return mapOfClients.get(name);
        
    }


    public  Account getAccount(long accId){
           
        return mapOfAccounts.get(accId);
        
    }
        
      public boolean containsName(String name){  
        
        return mapOfClients.containsKey(name);
      }


      public boolean containsAccountId(Long accId){  
        
        return mapOfAccounts.containsKey(accId);
      }
}
