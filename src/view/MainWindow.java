package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;


public class MainWindow {

    public static MainWindow instance = null;

    public static String content = "NINCS HiR";
    public static String lastInput = "";
public static JEditorPane jEditorPane = null;    
    
    
    
    public static void setContent(String webcontent) {
        content = webcontent;
    }

    public static MainWindow invokeConstructor() {

        if (instance == null) {
            return new MainWindow();
        }

        return instance;
    }



    private MainWindow() {
        SwingUtilities.invokeLater(new Runnable() {
            

            public void run() {


            // create jeditorpane
            jEditorPane = new JEditorPane();

            jEditorPane.setEditable(false);

            JScrollPane scrollPane = new JScrollPane(jEditorPane);

            HTMLEditorKit kit = new HTMLEditorKit();
            jEditorPane.setEditorKit(kit);

            StyleSheet styleSheet = kit.getStyleSheet();
            styleSheet.addRule("body {color:#000; font-family:times; margin: 4px; }");
            styleSheet.addRule("h1 {color: blue;}");
            styleSheet.addRule("h2 {font : 14px monaco; color: #880044;}");
            styleSheet.addRule("pre {font : 14px monaco; color : black; background-color : #99fa66; }");
            styleSheet.addRule("p {font : 16px monaco; color : black; background-color : #aaaaaa; }");
            styleSheet.addRule("span {font : 16px monaco; color : #ffaa00; background-color : #555555; }");

            String htmlString = "<html>\n"
                    + "<body>\n"
                    
                    + "</body>\n";



            Document doc = kit.createDefaultDocument();
            jEditorPane.setDocument(doc);
            jEditorPane.setText(htmlString);


              JLabel jInputLabel = new JLabel("INPUT");
              JTextField textField = new JTextField(16);
              JPanel jInputPanel = new JPanel();
              jInputPanel.add(jInputLabel);
              jInputPanel.add(textField);
              Action action = new AbstractAction()
{
    @Override
    public void actionPerformed(ActionEvent e)
    {


       lastInput = textField.getText();

        textField.setText("");
    }
};


textField.addActionListener( action );
                

            JFrame j = new JFrame("BANK OF GEEKS");
            Font font = new Font("DejaVu Sans Mono", Font.PLAIN, 24);
            textField.setFont(font);




scrollPane.scrollRectToVisible(new Rectangle(0,jEditorPane.getBounds(null).height,1,1));
            
            
            j.getContentPane().add(scrollPane, BorderLayout.CENTER);
            j.getContentPane().add(jInputPanel, BorderLayout.NORTH);

            j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            j.setSize(new Dimension(1000, 600));

            j.setLocationRelativeTo(null);
            j.setVisible(true);

    }
        });
}
    
    public static String wait4Input() throws InterruptedException{
        
        while( "".equals(lastInput)){
            Thread.sleep(300);

        }
        String input = lastInput.trim();
        lastInput = "";
        
        return input;
    }

    
    public static void clearWindow(){
        content = "";
       jEditorPane.setText(content);
    }
    
    
    
    public static void updateWindow(HtmlTag tag, String newcontent ){

        String tagPre;
        String tagPost;
        
        switch(tag){
            
            case H1:
                tagPre = "<h1>";
                tagPost="</h1>";
            break;
            case H2:
                tagPre = "<h2>";
                tagPost="</h2>";
            break;
            case P:
                tagPre = "<p>";
                tagPost="</p>";
            break;
            case PRE:
                tagPre = "<pre>";
                tagPost="</pre>";
            case SPAN:
                tagPre = "<span>";
                tagPost="</span>";
            break;
            default:
                tagPre = "";
                tagPost="";
            break;
        }
        
        
        
        
        
        content += tagPre + newcontent + tagPost;
        jEditorPane.setText(content);

int len = jEditorPane.getDocument().getLength();
jEditorPane.setCaretPosition(len);        
    }


    
    
}
