package view;

public interface FrameableText {

    public static final String ANSI_SUBSTRING = "\u001B[";

    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_GREEN_BG = "\u001B[42m";
    public static final String ANSI_RED_BG = "\u001B[41m";
    public static final String ANSI_YELLOW_BG = "\u001B[43m";
    public static final String ANSI_PURPLE_BG = "\u001B[44m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final char BLOCK = '\u2592';
    public static final char SPACER = ' ';
    
    
    default int countOccurrencesOfSubstring (String str, String subStrToFind){
        
        int count = 0, fromIndex = 0;
        
        while ((fromIndex = str.indexOf(subStrToFind, fromIndex)) != -1 ){
 
            count++;
            fromIndex++;
            
        }       
        
        return count;
    }
    

    default String convertTextToFramed(String msg) {

        String[] msgSplit = msg.split("\n");
        
        
        
        int maxLineLength = 0;
        for (int line = 0; line < msgSplit.length; line++) {


            if (msgSplit[line].length() > maxLineLength) {
                maxLineLength = msgSplit[line].length();
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append(addChar(40, BLOCK));
        sb.append("<br>\n");
        for (int currentLine = 0; currentLine < msgSplit.length; currentLine++) {


            
            sb.append(addChar(1, BLOCK));
            sb.append(msgSplit[currentLine]);
            int modifier = ( countOccurrencesOfSubstring(msgSplit[currentLine], ANSI_SUBSTRING)>2) ? 5 : 0;            

            
            int numOfSpacersNeeded = maxLineLength +9 +modifier -msgSplit[currentLine].length();
            sb.append(addChar(numOfSpacersNeeded, SPACER));
            sb.append(addChar(1, BLOCK));            
            sb.append("\n");

        }
        sb.append(addChar(40, BLOCK));
        sb.append("<br>\n");

        return sb.toString();
    }

    default StringBuilder addChar(int numOfDots, char toAdd) {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numOfDots; i++) {
            sb.append(toAdd);
        }
        return sb;
    }

    
    default String AccessLevelFramed (String text){
                
        return  convertTextToFramed(" BEJELENTKEZVE MINT: "+ text );
        
    }
    
}














