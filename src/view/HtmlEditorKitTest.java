package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;


public class HtmlEditorKitTest
{

  
  public HtmlEditorKitTest()
  {
    SwingUtilities.invokeLater(new Runnable()
    {
      public void run()
      {
        // create jeditorpane
        JEditorPane jEditorPane = new JEditorPane();
        
        // make it read-only
        jEditorPane.setEditable(false);
        
        // create a scrollpane; modify its attributes as desired
        JScrollPane scrollPane = new JScrollPane(jEditorPane);
        
        // add an html editor kit
        HTMLEditorKit kit = new HTMLEditorKit();
        jEditorPane.setEditorKit(kit);
        
        // add some styles to the html
        StyleSheet styleSheet = kit.getStyleSheet();
        styleSheet.addRule("body {color:#000; font-family:times; margin: 4px; }");
        styleSheet.addRule("h1 {color: blue;}");
        styleSheet.addRule("h2 {color: #880066;}");
        styleSheet.addRule("pre {font : 14px monaco; color : black; background-color : #99fa66; }");
       // styleSheet.addRule("img {background-color : #ff0000; }");

        // create some simple html as a string
        String htmlString = "<html>\n"
                          + "<body>\n"
                          + "<h1>ÜDVÖZÖLJÜK!</h1>\n"
                          + "<h2>Ez itt egy hasznos menü</h2>\n"
                          + "<h2>Ha segítséget keres kattintson a linkre</h2>\n"
                          + "<pre>KOCKÁK BANKJA - NÁLUNK NINCS ÁRFOLYAMKOCKÁZAT</pre>\n"
                          + "<h2>0% Kezelési költség!</h2>\n"
                          + "<p><a href=\"http://kockabank.hu/\">kockabank.hu</a></p>\n"

                          + "</body>\n";
        
        // create a document, set it on the jeditorpane, then add the html
        Document doc = kit.createDefaultDocument();
        jEditorPane.setDocument(doc);
        jEditorPane.setText(htmlString);

        // now add it all to a frame
        JFrame j = new JFrame("HtmlEditorKit Test");
        j.getContentPane().add(scrollPane, BorderLayout.CENTER);

        // make it easy to close the application
        j.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        // display the frame
        j.setSize(new Dimension(550,350));
        
        // pack it, if you prefer
        //j.pack();
        
        // center the jframe, then make it visible
        j.setLocationRelativeTo(null);
        j.setVisible(true);
      }
    });
  }
}
