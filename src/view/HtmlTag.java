package view;

/**
 *
 * @author mantis
 */
public enum HtmlTag {
    H1, H2, PRE, P, SPAN
}
