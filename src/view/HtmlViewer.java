package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import main.Main;

public class HtmlViewer {

    public static HtmlViewer instance = null;

    public static String content = "NINCS HiR";

    public static void setContent(String webcontent) {
        content = webcontent;
    }

    public static HtmlViewer invokeConstructor() {

        if (instance == null) {
            return new HtmlViewer();
        }

        return instance;
    }


    private HtmlViewer() {
        SwingUtilities.invokeLater(new Runnable() {
            

            public void run() {


        try {
            JEditorPane jEditorPane = new JEditorPane();

            jEditorPane.setEditable(false);

            JScrollPane scrollPane = new JScrollPane(jEditorPane);

            HTMLEditorKit kit = new HTMLEditorKit();
            jEditorPane.setEditorKit(kit);

            StyleSheet styleSheet = kit.getStyleSheet();
            styleSheet.addRule("body {color:#000; font-family:times; margin: 4px; }");
            styleSheet.addRule("h1 {color: blue;}");
            styleSheet.addRule("h2 {color: #aa0066;}");
            styleSheet.addRule("pre {font : 12px monaco; color : black; background-color : #99fa66; }");

            String htmlString = "<html>\n"
                    + "<body>\n"
                    + "<h1>VIDD HÍRÜNKET!</h1>\n"
                    + "<h2>Ajánld bankunkat egy barátodnak</h2>\n"
                    + "<h2>Ha regisztrál, mindketten kaptok</h2>\n"
                    + "<h1>1000 forintot!</h1>\n"
                    + "<pre>RÉSZLETEK:</pre>\n"
                    + "<h2>www.kockabank.hu</h2>\n"
                    + content
                    + "</body>\n";

            String path4 = "./assets/refer-earn.png";
            File file4 = new File(path4);
            BufferedImage image4 = ImageIO.read(file4);
            JLabel label4 = new JLabel(new ImageIcon(image4));
            JPanel jpanelMain2 = new JPanel();
            jpanelMain2.add(label4);

            Document doc = kit.createDefaultDocument();
            jEditorPane.setDocument(doc);
            jEditorPane.setText(htmlString);

            JFrame j = new JFrame("Promoció");
            j.getContentPane().add(jpanelMain2, BorderLayout.WEST);
            j.getContentPane().add(scrollPane, BorderLayout.CENTER);

            j.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

            j.setSize(new Dimension(1000, 800));


            j.setLocationRelativeTo(SwingGui.jpanelMain);
            j.setVisible(true);
        } catch (IOException ex) {
            Logger.getLogger(HtmlViewer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        });
}
}
