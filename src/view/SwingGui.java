package view;

import control.MainMenu;
import javax.swing.UIManager.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import static view.SwingGui.jpanel;
import static view.SwingGui.window;

public class SwingGui extends JFrame
        implements ActionListener {

    static JTextArea userName;
    static JTextArea password;
    public static JTextArea lists;
    static JLabel jErrorDispl;

    static JPanel jpanel;
    static JPanel jpanelChart;
    static JPanel jpanelChart2;
    static JPanel jpanelTextarea;
    static JPanel jpanelMain;
    static JPanel jpanelDespair;
    
    public static boolean isClicked = false;

    static JMenuBar menuBar = new JMenuBar();
    static JMenu menuMain = new JMenu("Fooldal");
    static JMenu menuLogin = new JMenu("Belepes");

    static JMenu menuChart = new JMenu("Grafikon");

    static JMenuItem menuMiniApp = new JMenu("Valuta válto");
    static JMenu menuHelp = new JMenu("Segitseg");
    static JMenuItem menuHelpHtml = new JMenu("Sugo");
    static JMenuItem menuFAQ = new JMenu("GY.I.K.");
    static JMenuItem menuAbout = new JMenu("Nevjegy");
    static JMenuItem menuGame = new JMenu("FUN&GAMES");
    static JMenu menuExit = new JMenu("Kilep");

    static JComboBox convertFrom, convertTo;
    static JTextField From, To;
    static JButton compute, buttonExitMiniApp;
    static JLabel from, to;
    static JLabel lblFrom, lblTo;
    static double input = 0d;
    static double result = 0d;

    static JFrame window = new JFrame();
    static JPanel cardPanel1 = new JPanel();
    static JPanel miniAppPanel = new JPanel();
    static JPanel menuPanel = new JPanel();

    public SwingGui() {


        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        menuPanel.add(menuBar);

        menuBar.add(menuMain);
        menuBar.add(menuLogin);
        menuBar.add(menuChart);

        menuBar.add(menuMiniApp);
        menuBar.add(menuHelp);
        menuHelp.add(menuHelpHtml);
        menuHelp.add(menuFAQ);
        menuHelp.add(menuAbout);
        menuHelp.add(menuGame);

        menuBar.add(menuExit);
        menuExit.setMnemonic(KeyEvent.VK_K);

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLayout(new BorderLayout());
        window.setTitle("Bank of Geeks Start Menu");
        window.setLocationRelativeTo(window);
        window.setLocation(150, 150);

        window.setResizable(true);
        window.getContentPane().add(menuPanel, BorderLayout.PAGE_START);
        window.getContentPane().add(jpanelMain, BorderLayout.CENTER);
        window.pack();
        window.setVisible(true);

        cardPanel1.setBackground(Color.DARK_GRAY);
        miniAppPanel.setBackground(Color.lightGray);


        menuMain.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                HtmlViewer.invokeConstructor();
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {

                
                
                SwingGui.window.getContentPane().removeAll();
                SwingGui.window.getContentPane().add(SwingGui.menuPanel, BorderLayout.PAGE_START);
                SwingGui.window.getContentPane().add(SwingGui.jpanelMain, BorderLayout.CENTER);
                SwingGui.window.revalidate();
                SwingGui.window.repaint();


            }
        });
        menuLogin.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {

                SwingGui.isClicked = true;

                try {
                    // SwingGui.window.pack();
                new Thread((Runnable) new MainMenu()).start();
                } catch (Exception ex) {
                    Logger.getLogger(SwingGui.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        menuChart.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {

                SwingGui.window.getContentPane().remove(SwingGui.jpanelMain);
                SwingGui.window.getContentPane().remove(SwingGui.jpanelDespair);
                SwingGui.window.getContentPane().add(SwingGui.jpanelChart, BorderLayout.PAGE_START);
                SwingGui.window.getContentPane().add(SwingGui.jpanelChart2, BorderLayout.CENTER);
                SwingGui.window.revalidate();
                SwingGui.window.repaint();
                SwingGui.window.pack();
            }
        });
        menuMiniApp.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                SwingGui.window.getContentPane().removeAll();
                SwingGui.window.getContentPane().add(SwingGui.miniAppPanel, BorderLayout.CENTER);

                SwingGui.window.revalidate();
                SwingGui.window.repaint();
                System.out.println("debug panel changed");
            }

        });

        menuHelpHtml.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

                SwingGui.window.getContentPane().remove(SwingGui.jpanelMain);
                SwingGui.window.getContentPane().remove(SwingGui.jpanelChart);
                SwingGui.window.getContentPane().remove(SwingGui.jpanelChart2);
                SwingGui.window.getContentPane().remove(SwingGui.jpanelTextarea);
                SwingGui.window.getContentPane().add(SwingGui.jpanelDespair, BorderLayout.CENTER);
                SwingGui.window.revalidate();
                SwingGui.window.repaint();
                SwingGui.window.pack();
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                //JOptionPane.showMessageDialog(getRootPane(), "Bank Of Geeks!", " Információ", JOptionPane.INFORMATION_MESSAGE);

                new HtmlEditorKitTest();
            }

        });
        menuAbout.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                JOptionPane.showMessageDialog(null, "Bank Of Geeks!", " Információ", JOptionPane.INFORMATION_MESSAGE);
            }

        });

        menuFAQ.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                menuFAQ.removeMouseListener(this);
                JOptionPane.showMessageDialog(null, "Kérem fáradjon be személyesen bankfiókunkba, az Ön személyes ügyintézője Rakita Csaba", " Információ", JOptionPane.INFORMATION_MESSAGE);

            }
        });

        menuGame.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                window.setVisible(false);
                window.getContentPane().removeAll();
                window.repaint();

                menuMiniApp.removeMouseListener(this);
                menuHelp.removeMouseListener(this);
                menuHelpHtml.removeMouseListener(this);
                menuFAQ.removeMouseListener(this);
                menuAbout.removeMouseListener(this);
                menuExit.removeMouseListener(this);

                window.dispose();
                System.gc();

                new Thread((Runnable) new HoppingDino()).start();
            }
        });

        menuExit.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (JOptionPane.showConfirmDialog(null, "Biztos benne, hogy kilép ? ", "Megerősítés",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });

        lblFrom = new JLabel("From:");
        lblFrom.setBounds(30, 45, 250, 30);
        lblFrom.setForeground(Color.RED);
        miniAppPanel.add(lblFrom);
        convertFrom = new JComboBox();
        convertFrom.setBounds(30, 70, 150, 30);
        convertFrom.addItem("Hungarian Forint");
        convertFrom.addItem("USA Dollar");
        convertFrom.addItem("EURO");
        miniAppPanel.add(convertFrom);
        lblTo = new JLabel("To:");
        lblTo.setBounds(230, 45, 250, 30);
        lblTo.setForeground(Color.RED);
        miniAppPanel.add(lblTo);
        convertTo = new JComboBox();
        convertTo.setBounds(230, 70, 150, 30);
        convertTo.addItem("Hungarian Forint");
        convertTo.addItem("USA Dollar");
        convertTo.addItem("EURO");
        miniAppPanel.add(convertTo);

        from = new JLabel("Add meg az átváltani kívánt összeget:");
        from.setBounds(50, 110, 300, 30);
        miniAppPanel.add(from);
        From = new JTextField(15);
        From.setBounds(50, 140, 300, 30);
        miniAppPanel.add(From);

        compute = new JButton("Átváltás");
        compute.setBounds(100, 250, 100, 30);
        miniAppPanel.add(compute);
        to = new JLabel("Az összeg amit átváltottál:");
        to.setBounds(50, 170, 300, 30);
        miniAppPanel.add(to);
        To = new JTextField(15);
        To.setBounds(50, 200, 300, 30);
        To.setEditable(false);
        To.setForeground(Color.RED);
        miniAppPanel.add(To);
        buttonExitMiniApp = new JButton("Exit");
        buttonExitMiniApp.setBounds(210, 250, 100, 30);
        miniAppPanel.add(buttonExitMiniApp);
        convertFrom.addActionListener(this);
        convertTo.addActionListener(this);
        compute.addActionListener(this);
        From.addActionListener(this);
        buttonExitMiniApp.addActionListener(this);

        SwingGui.window.setVisible(true);

    }

    private void exitMessage() {

        if (JOptionPane.showConfirmDialog(this, "Biztos benne, hogy kilép ? ", "Megerősítés", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

            System.exit(0);

        }

        dispose();

        System.exit(0);

    }

    private void infoMessage(String message) {

        JOptionPane.showMessageDialog(this, message, " Információ", JOptionPane.INFORMATION_MESSAGE);

    }

    public static void initGUI() throws IOException {
        String path = "./assets/HK_Promo_nyar_744x278.jpg";
        String path2 = "./assets/grafikon.png";
        String path3 = "./assets/grafikon2.png";
        String path4 = "./assets/despair.jpg";
        File file = new File(path);
        BufferedImage image = ImageIO.read(file);
        JLabel label = new JLabel(new ImageIcon(image));
        File file2 = new File(path2);
        BufferedImage image2 = ImageIO.read(file2);
        JLabel label2 = new JLabel(new ImageIcon(image2));
        File file3 = new File(path3);
        BufferedImage image3 = ImageIO.read(file3);
        JLabel label3 = new JLabel(new ImageIcon(image3));
        File file4 = new File(path4);
        BufferedImage image4 = ImageIO.read(file4);
        JLabel label4 = new JLabel(new ImageIcon(image4));

        jpanel = new JPanel();
        jpanelMain = new JPanel();
        jpanelChart = new JPanel();
        jpanelChart2 = new JPanel();
        jpanelDespair = new JPanel();
        jpanelTextarea = new JPanel();

        jpanelMain.add(label);
        jpanelChart.add(label2);
        jpanelChart2.add(label3);
        jpanelDespair.add(label4);
        
        Font font = new Font("Monospaced", Font.PLAIN, 16);
        Font font2 = new Font("DejaVu Sans Mono", Font.PLAIN, 24);
        userName = new JTextArea(1, 12);
        userName.setFont(font);
        password = new JTextArea(1, 12);
        password.setText("pass");
        password.setFont(font);
        lists = new JTextArea(40, 60);
        lists.setFont(font);
        lists.setText("\n\n\n\nA belépéshez térj vissza a KONZOL-ra");
        jpanelTextarea.add(lists);

    }

    public static void initDisplay() throws IOException, InterruptedException {
        initGUI();
        new SwingGui();

    }

    @Override
    public void actionPerformed(ActionEvent e) {


        if (e.getSource() == buttonExitMiniApp) {
            SwingGui.window.getContentPane().removeAll();
            SwingGui.window.getContentPane().add(SwingGui.menuPanel);
            // SwingGui.window.getContentPane().add(MenuUC.buttonPanel, BorderLayout.CENTER);
            SwingGui.window.getContentPane().add(SwingGui.jpanelMain, BorderLayout.CENTER);
            SwingGui.window.revalidate();
            SwingGui.window.repaint();

        }

        if (e.getSource() == compute) {

            try{
            input = Double.parseDouble(From.getText());
            String msg = From.getText();
            if (convertFrom.getSelectedItem() == "Hungarian Forint") {
                ConvertHungarianForint();
            } else if (convertFrom.getSelectedItem() == "USA Dollar") {
                ConvertUSDollar();
            } else {
                ConvertEur();
            }
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(null, "Hibás adattípus, Újra kérem" , "HIBA", JOptionPane.ERROR_MESSAGE);
            }
        }


    }

    public void exit() {

        setVisible(false);
        dispose();
        System.exit(0);

    }

    public void ConvertHungarianForint() {

        if (convertTo.getSelectedItem() == "USA Dollar") {
            result = ((double) input / 265.5);
            To.setText("" + result);
        } else if (convertTo.getSelectedItem() == "EURO") {
            result = ((double) input / 322.4);
            To.setText("" + result);
        } else {
            result = (input);
            To.setText("" + result);

        }
    }

    public void ConvertUSDollar() {

        if (convertTo.getSelectedItem() == "Hungarian Forint") {

            result = (input * 265.5);

            To.setText("" + result);
        } else if (convertTo.getSelectedItem() == "EURO") {

            result = (input * (1/1.33));
            To.setText("" + result);

        } else {

            result = (input);
            To.setText("" + result);
        }
    }

    public void ConvertEur() {

        if (convertTo.getSelectedItem() == "Hungarian Forint") {

            result = (input * 322.4);
            To.setText("" + result);
        } else if (convertTo.getSelectedItem() == "USA Dollar") {

            result = (input * 1.33);
            To.setText("" + result);
        } else {
            result = (input);
            To.setText("" + result);
        }

    }

}
