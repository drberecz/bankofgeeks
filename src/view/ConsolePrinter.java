/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import control.MainMenu;
import control.utils.HttpConnection;
import database.DataBase;

import java.util.List;
import java.util.Map;

import model.Account;
import model.pojos.Client;
import model.pojos.Currency;
import model.pojos.Euro;
import model.pojos.TransferUnit;
import model.pojos.UsaDollar;

public class ConsolePrinter implements FrameableText {

    
    public static MainWindow mw = MainMenu.mw;
    
    
    public static void printListOfClientACCS(Client client) {
        
        mw.clearWindow();
        
        StringBuilder sb = new StringBuilder();
        sb.append("   \u00dcGYF\u00c9L: ").append(client.getName()).append(" - AKT\u00cdV SZ\u00c1ML\u00c1INAK LIST\u00c1JA<br>");
        List<Account> accountList = client.getAccounts();
        for (Account account : accountList) {
            String currencyCode = account.getCurrency().getCode();
           // String HIGHLIGHTER = (account.isActive()) ? FrameableText.ANSI_GREEN_BG : FrameableText.ANSI_RED_BG;
            String status = (account.isActive()) ? "Aktiv"  : "Letiltva";
            sb.append("accountID: ").append(account.getaccountId()).append(" | ");
            sb.append("egyenleg: ").append(account.getBalance()).append(" ").append(currencyCode);
            sb.append(" ").append("<span>").append(status).append("</span>");
            sb.append("<br>");
        }
        mw.updateWindow(HtmlTag.PRE, client.convertTextToFramed(sb.toString()));
    }

    public static void printTransactionHistory(Client currentClient) {
        
        List<Account> accounts = currentClient.getAccounts();
        
        mw.updateWindow(HtmlTag.SPAN, "ÜGYFÉL: " + currentClient.getName() +
                " SZÁMLATÖRTÉNETE:<br>"
                );
        
        for (Account acc : accounts) {
            List<TransferUnit> logBook = acc.getLogBook();
            for (TransferUnit unit : logBook) {
               mw.updateWindow(HtmlTag.P,unit.toString());
            }
        }
    }

    public static void printMeanRatesEURUSD() {
        Currency eur = new Euro();
        Currency usd = new UsaDollar();
        System.out.println(ANSI_YELLOW + ANSI_PURPLE_BG + "********************");
        System.out.println("NAPI ÁRFOLYAMOK:    ");
        System.out.println("EUR: " + eur.getValueInHUF());
        System.out.println("USD: " + usd.getValueInHUF());
        System.out.println("********************" + ANSI_RESET);

    }

    public static void printStartScreen() throws Exception {
        List<String> webContent = HttpConnection.getWebsiteContentasList("http://hidegver.nhely.hu/BankOfGeeks/BankTitleScreen.txt");
        System.out.println();
        for (String item : webContent) {
            System.out.print(item);
            Thread.sleep(100);
        }
    }

    public static void printClients(DataBase dBase) {
            
            Map<String,Client> mapOfClients = dBase.getClientMap();
            
        for (Map.Entry<String, Client> entry : mapOfClients.entrySet()) {
            System.out.printf("Neve : %s , %s %n", entry.getKey(), entry.getValue());
        }
    }

    public static void printAccounts(DataBase dBase) {
        StringBuilder sb = new StringBuilder();
            Map<Long,Account> mapOfAccounts = dBase.getAccountMap();

        for (Map.Entry<Long, Account> entry : mapOfAccounts.entrySet()) {
            sb.append("Szamlaszam: ").append(entry.getKey()).append(" | ").append(entry.getValue().toString()).append("\n");
        }
        System.out.println(dBase.convertTextToFramed(sb.toString()));
    }
}
