package control.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class FileIO {

    static final String FILE_PATH = "./database/";


    static String[] getDirectory() {
        File dir = new File(FILE_PATH);
        System.out.println("ebben keres: " + dir.getAbsolutePath());

        File file = null;
        String[] paths = new String[]{};

        try {

            file = new File(FILE_PATH);


            paths = file.list();

            System.out.println("EZ A Konyvtar tartalma:");

            for (String path : paths) {

                System.out.println(path);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return paths;
    }

    public static void updateFile(Object obj, String fileName) {

        System.out.println("Most akkor beleirom a file-ba");

        writeFile(obj, fileName);

    }



    public static void createDir() {

        String dirName = FILE_PATH;
        File newDir = new File(dirName);
        newDir.mkdirs();
        System.out.println("Uj konytar letrehozva : " + FILE_PATH);
    }

    public static void writeFile(Object toBeUpdated, String fileName) {
        
        try {
            FileOutputStream fileOut
                    = new FileOutputStream(FILE_PATH + fileName);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(toBeUpdated);
            out.close();
            fileOut.close();

        } catch (IOException i) {
            System.out.println("iras NEM SIKERULT");
        }
    }

    
    public static Object readFile(String fileName) {
        Object inputObject = new Object();
        try {
            FileInputStream fileIn = new FileInputStream(FILE_PATH + fileName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            inputObject = (Object) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            System.out.println("FILE I/O nem okes / empty file?");
        } catch (ClassNotFoundException c) {
            System.out.println("Meg nincs meg a FILE");

        }

        return inputObject;
    }

}







