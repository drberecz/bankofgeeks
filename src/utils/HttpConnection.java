package control.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;


public class HttpConnection {

    private final String USER_AGENT = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/71.0.3578.98 Chrome/71.0.3578.98 Safari/537.36";

    //https://mnb.hu/arfolyamok/

     public List<String> sendGet(String url) throws Exception {
             
    List<String> response = new ArrayList<>(); 

        System.out.println(url);
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.9");

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;

        
        while ((inputLine = in.readLine()) != null) {
            inputLine +="\n";
            response.add(inputLine);
if ( inputLine.contains("middleRate")) System.out.println(inputLine);
        }
        in.close();

    return response;
    }
    
    public static List<String> getWebsiteContentasList(String url) throws Exception {
        HttpConnection test = new HttpConnection();
        List<String> webPageHTML = test.sendGet(url);
        return webPageHTML;
        
    }
     
          
}
