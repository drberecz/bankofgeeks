# BankOfGeeks

Java SE7 desktop app, a "primer" into Agile development.
Completed this relatively small project in a rotational system(kept changing roles) 4 team members

Features:

- Home grown persistent layer via object serialization
- Consumes RESTful web service via Http connection
- Custom XML parser service (schema was not available)
- Java Swing user interface displaying HTML 3.0 elements
